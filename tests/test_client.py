# Copyright (C) 2023 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import hydrus_api
import hydrus_api.utils


def get_api_credentials() -> tuple[str, str]:
    if not (api_key := os.getenv("HYDRUS_API_KEY")):
        raise RuntimeError("HYDRUS_API_KEY environment variable must be set (see .env file)")

    api_url = os.getenv("HYDRUS_API_URL", hydrus_api.DEFAULT_API_URL)
    return api_key, api_url


# Just a very basic, definitely non-exhaustive test to make sure something isn't majorly broken
def test_client() -> None:
    api_key, api_url = get_api_credentials()
    client = hydrus_api.Client(api_key, api_url)
    print(client.get_api_version())
    print(client.get_session_key())
    print(client.verify_access_key())
    print(client.get_services())
    print(client.clean_tags(["hello world", "hello", "world"]))
    print(client.get_url_files("https://hydrusnetwork.github.io/"))
    print(client.get_url_info("https://hydrusnetwork.github.io/"))
    print(client.get_cookies("hydrusnetwork.github.io"))
    print(client.get_mr_bones())
    print(client.get_pages())
    print(hydrus_api.utils.get_page_list(client))
