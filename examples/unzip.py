#!/usr/bin/env python3

# Copyright (C) 2023 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import enum
import pathlib
import shutil
import tempfile
import zipfile

import hydrus_api
import hydrus_api.utils

REQUIRED_PERMISSIONS = (hydrus_api.Permission.IMPORT_FILES, hydrus_api.Permission.ADD_TAGS)


class ExitCode(enum.IntEnum):
    SUCCESS = 0
    FAILURE = 1


def get_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("access_key")
    parser.add_argument("--search-tag", "-s", dest="search_tags", action="append", default=["system:filetype is zip"])
    parser.add_argument("--file-service", "-f", dest="file_services", action="append")
    parser.add_argument("--tag-service", "-t", default="my tags")
    parser.add_argument("--api-url", "-a", default=hydrus_api.DEFAULT_API_URL)
    return parser


def run(arguments: argparse.Namespace) -> ExitCode:
    client = hydrus_api.Client(arguments.access_key, arguments.api_url)
    if not hydrus_api.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ExitCode.FAILURE

    # Translate passed file- and tag-service keys or names into keys. If there are multiple services with the same name
    # we just take the first one
    service_mapping = hydrus_api.utils.get_service_mapping(client)
    if arguments.file_services:
        file_service_keys = [
            service_mapping[name_or_key][0] if name_or_key in service_mapping else name_or_key
            for name_or_key in arguments.file_services
        ]
    else:
        file_service_keys = None
    tag_service_key = (
        service_mapping[arguments.tag_service][0] if arguments.tag_service in service_mapping else arguments.tag_service
    )

    zip_path = pathlib.Path(tempfile.mkstemp()[1])
    file_ids = client.search_files(
        arguments.search_tags, file_service_keys=file_service_keys, tag_service_key=tag_service_key
    )["file_ids"]
    for file_id in file_ids:
        metadata = client.get_file_metadata(file_ids=[file_id])["metadata"][0]
        tags = metadata["tags"][tag_service_key]["storage_tags"][str(hydrus_api.TagStatus.CURRENT)]

        print("Downloading", metadata["hash"])
        response = client.get_file(file_id=file_id)
        zip_path.write_bytes(response.content)
        zip_dir = pathlib.Path(tempfile.mkdtemp())
        print("Extracting...")
        with zipfile.ZipFile(zip_path) as file:
            try:
                file.extractall(zip_dir)
            except RuntimeError as error:
                print(f"Error extracting {metadata['hash']}:", error)

        file_paths = tuple(path for path in zip_dir.rglob("*") if path.is_file())
        if file_paths:
            print("Adding and tagging", len(file_paths), "files")
            hydrus_api.utils.add_and_tag_files(client, file_paths, tags, (tag_service_key,))

        shutil.rmtree(zip_dir, ignore_errors=True)

    zip_path.unlink(missing_ok=True)
    return ExitCode.SUCCESS


def main() -> None:
    parser = get_argument_parser()
    arguments = parser.parse_args()
    parser.exit(run(arguments))


if __name__ == "__main__":
    main()
