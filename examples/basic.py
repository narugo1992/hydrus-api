#!/usr/bin/env python3

# Copyright (C) 2023 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pprint
import sys
import time
import enum

import hydrus_api
import hydrus_api.utils

NAME = "Basic Test"
REQUIRED_PERMISSIONS = (
    hydrus_api.Permission.IMPORT_URLS,
    hydrus_api.Permission.IMPORT_FILES,
    hydrus_api.Permission.ADD_TAGS,
    hydrus_api.Permission.SEARCH_FILES,
    hydrus_api.Permission.MANAGE_PAGES,
)


class ExitCode(enum.IntEnum):
    SUCCESS = 0
    FAILURE = 1


def main() -> ExitCode:
    client = hydrus_api.Client()
    print(f"Client API version: v{client.VERSION} | Endpoint API version: v{client.get_api_version()['version']}")

    api_key = hydrus_api.utils.cli_request_api_key(NAME, REQUIRED_PERMISSIONS)
    client = hydrus_api.Client(api_key)
    if not hydrus_api.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        sys.exit(ExitCode.FAILURE)

    url_info = client.get_url_info("https://hydrusnetwork.github.io/hydrus/help/client_api.html")
    print(url_info)

    all_file_ids = client.search_files(["test"])["file_ids"]
    for file_ids in hydrus_api.utils.yield_chunks(all_file_ids, 100):
        pprint.pprint(client.get_file_metadata(file_ids=file_ids))

    print(client.get_session_key())
    for page in hydrus_api.utils.get_page_list(client):
        print(page)
        if page["name"] == "top page notebook":
            continue

        client.focus_page(page["page_key"])
        pprint.pprint(client.get_page_info(page["page_key"]))
        time.sleep(1)

    return ExitCode.SUCCESS


if __name__ == "__main__":
    sys.exit(main())
