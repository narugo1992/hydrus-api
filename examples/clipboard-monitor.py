#!/usr/bin/env python3

# Copyright (C) 2023 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import subprocess
import time
import collections.abc as abc
import enum

import pyperclip

import hydrus_api
import hydrus_api.utils

REQUIRED_PERMISSIONS = (hydrus_api.Permission.IMPORT_URLS, hydrus_api.Permission.MANAGE_PAGES)


class ExitCode(enum.IntEnum):
    SUCCESS = 0
    FAILURE = 1


def get_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("api_key")
    parser.add_argument("--interval", "-i", type=float, default=0.1)
    parser.add_argument("--api-url", "-a", default=hydrus_api.DEFAULT_API_URL)
    return parser


def dmenu(options: abc.Iterable[str]) -> str:
    process = subprocess.run(
        ("dmenu",),
        input="\n".join(options),
        capture_output=True,
        universal_newlines=True,
    )
    return process.stdout.strip()


def get_downloader_pages(client: hydrus_api.Client) -> list[str]:
    return [
        page["name"]
        for page in hydrus_api.utils.get_page_list(client)
        if page["page_type"] in (hydrus_api.PageType.THREAD_WATCHER, hydrus_api.PageType.URL_DOWNLOADER)
    ]


def main(arguments: argparse.Namespace) -> ExitCode:
    client = hydrus_api.Client(arguments.api_key, arguments.api_url)
    if not hydrus_api.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ExitCode.FAILURE

    old_contents = pyperclip.paste()
    while True:
        contents = pyperclip.paste()
        if contents == old_contents:
            time.sleep(arguments.interval)
            continue

        old_contents = contents

        try:
            url_info = client.get_url_info(contents)
        except hydrus_api.MissingParameter:
            print("Unrecognized URL:", repr(contents))
            time.sleep(arguments.interval)
            continue

        pages = get_downloader_pages(client)
        candidate_count = len(pages)
        if candidate_count > 1:
            page_name = dmenu(pages)
            if not page_name:
                continue
        elif candidate_count == 1:
            page_name = pages[0]
        else:
            page_name = None

        normalized_url = url_info["normalised_url"]
        client.add_url(normalized_url, destination_page_name=page_name)
        print("Added", normalized_url, "to page", repr(page_name))
        time.sleep(arguments.interval)


if __name__ == "__main__":
    parser = get_argument_parser()
    arguments = parser.parse_args()
    try:
        parser.exit(main(arguments))
    except KeyboardInterrupt:
        parser.exit(ExitCode.SUCCESS)
